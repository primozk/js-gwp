 module.exports.permissions = {
    /* REST api for direct mongo access */
    'dbapi': false,               // DENY API access by default
    'dbapi._newid': true,         // anyone can retrieve a new id from mongo

    /* API for content management system */
    'cms': false,
    'cms._.all': false,               // get all cms articled
    'cms.published.read': true,       // read a published cms article
    'cms._.save': false,              // save changes to an article
    'cms._.publish': false,           // publish an article to make it publically accessible
    'cms._.delete': false,            // mark an article as deleted
    'cms._.purge': false,             // delete of articles of the same code

    /* call ebus services */
    'ebus': false,
    'ebus.hello': true,               // call service hello
    'ebus.sms.send': true,            // allow sending sms-es via smsi.si api (service sms, command send)
    'ebus.db': true,                  // can call ebus db service ()

    /* user manager */
    'users.admin.init': true,         // can add admin user if it doesn't exist
    'users.admin.edit': false,        // can administer users

    /* individual services */
    'hello': true,
    'db.article': true,
    'db.article.deleteOne': false,
};
