module.exports = [
    { title: "vip", to: "/vip" },
    { title: "helpdesk", to: "/helpdesk" },
    { title: "checkout", to: "/fpc" },
    { title: "ebus", to: "/callworker", more: true, private:true },
    { title: "ebus simple", to: "/callworkersimple", more: true, private:true },
    { title: "rest", to: "/rest", more: true },
  ];
