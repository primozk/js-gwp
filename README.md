# gwp

> Generic Web Portal

## install dependencies
$ npm install

## serve with hot reload at localhost:3200
$ npm run dev


# Changing things

*To add a new page* add mypage.vue to /pages directory

*To add external authentication* search for EXTERNAL_AUTH; implement your authentication in /server/auth-external.js and then uncomment line in /server/index.js

*To change permissions* (authorization) edit requiredPermissions.json - changes to this file are effective immediatly.

*To change system settings* edit .env file or set envieronment variables.


# Before going to production

Remove all unused examples:
/code/rte 
/code/countries
/code/rte.js
/components/fpc
/components/logo.vue
/components/serverStatus.vue
/components/vip.vue
/pages/callWorker.vue
/pages/callWorkerSimple.vue
/pages/dbtest.vue
/pages/fpc.vue
/pages/helpdesk.vue
/pages/login.vue  -  remove also lines from server/index.js (search for 'AUTH authentification middleware')
/pages/rest.vue
/pages/vip.vue
/plugins/google-maps.js

## Removing build-in cms server 

Remove
/pages/_cms  
/static/redactor
also delete lines from server/index.js (search for 'CMS api server')

