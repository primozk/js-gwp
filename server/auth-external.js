/*
    this koa middleware handles authentification other than jwt
    if authentification is successful, user object must be written into ctx.state.user
*/

/*
    EXTERNAL_AUTH
*/

module.exports = (opts = {}) => {
    const middleware = async function (ctx, next) {

        // get user object somehow
        let user = {
            username: 'jaka_racman',             // mandatory
            email: '',                          // optional
            name: 'dr. Jaka Racman',            // optional
            status: 1,                          // optional; if evals to false, the user will be removed later
            permissions: {                      // optional, but without permissons all authorizations will fail
                dbapi: true,
                cms: true,
                'services.hello': true,
            },
        };

        // assign user object
        ctx.state.user = user;

        // run other middleware
        await next();

        // jwtauth middleware will create jwt token; it can be accessed now
        // note that ctx.state.user may be {} is user's status was invalid (!!user.status==false of canLogIn(user) returned false)
        let token = ctx.state.user.token;
    }
    return middleware;
}
