/*
    this koa middleware:
    - if ctx.state.user is already present
        - creates a JWT token and stores it into ctx.state.user.token
    - otherwise, looks for a JWT token in authentication header or 'auth' cookie, then
        - decodes JWT token into ctx.state.user
        - stores JWT token  into ctx.state.user.token
*/

let jsonwebtoken = require('jsonwebtoken');
let decodeuser = require('../code/decodeuser');

function resolveAuthorizationHeader(ctx) {
    if (!ctx.header || !ctx.header.authorization) {
        return;
    }
    const parts = ctx.header.authorization.split(' ');
    if (parts.length === 2) {
        const scheme = parts[0];
        const credentials = parts[1];

        if (/^Bearer$/i.test(scheme)) {
            return credentials;
        }
    }
};

function resolveCookies(ctx) {
    return ctx.cookies.get('auth');
};

module.exports = (opts = {}) => {
    const middleware = async function (ctx, next) {
        let token = ctx.state.user || resolveAuthorizationHeader(ctx) || resolveCookies(ctx);
        ctx.state.user = decodeuser(token, opts) || {};
        await next();
    }
    return middleware;
}
