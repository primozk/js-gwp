/*
    initializes enterprise bus client
    monitors broker status
    reports broker status to socket.io, if available in proces.status.io
*/

// enterprise bus ebusclient
const Client = require('cry-ebus').Client;
const ebusclient = new Client();
process.status = process.status || {};
process.status.ebus = process.status.ebus || {};
process.status.ebus.client = ebusclient;
process.status.ebus.online = false;

const Can = require('cry-can');
let canServe = new Can('ebus');

// emit enterprise bus status to attached clients
ebusclient.on('online', () => {
	process.status.ebus.online = true;
	if (process.status.io) {
		process.status.io.emit('ebus online')
	}
})
ebusclient.on('offline', () => {
	process.status.ebus.online = false;
	if (process.status.io) {
		process.status.io.emit('ebus offline')
	}
})
ebusclient.on('message',(msg,channel) => {
    console.log('ebus message',channel,msg,!!process.status.io, !!process.status.io.clientAnnounce)
    if (process.status.io && process.status.io.clientAnnounce) {
        process.status.io.clientAnnounce(channel,msg);
    }
})
ebusclient.subscribe('')
ebusclient.start()

process.status.ebus.serve = async function(service,data,user={ permissions: {}}) {
    try {
        let operation = (data && data.operation) || 'call'
        let canExec = canServe.can(process.status.requiredPermissions, user.permissions, operation, service, 'ebus');
        if (data && data.collection) {
            canExec = canExec && canServe.can(process.status.requiredPermissions, user.permissions, operation, data.collection, service)
        }
        if (canExec) {
            return await process.status.ebus.client.requestPromise(service, data)
        } else {
            console.error('not allowed to call ebus service ',service, data)
            return null;
        }
    } catch(err) {
        console.error('error in process.status.ebus.serve',err.message)
    }
};

process.status.ebus.subscribe = async function(channel,callback,user={ permissions:{}}) {
    try {
        let canExec = canServe.can(process.status.requiredPermissions, user.permissions, data.channel, 'subscribe', 'ebus');
        if (canExec) {
            ebusclient.on(channel,callback);
        } else {
            console.error('user not authorized to subscribe ',user,data)
        }
    } catch(err) {
        console.error('error in process.status.ebus.subscribe',err.message)
    }
};

process.on('SIGTERM', function () {
    ebusclient.stop()
    process.exit(0);
});

process.on('SIGINT', function () {
    ebusclient.stop()
    process.exit(0);
});
