
const Koa = require('koa')
const helmet = require('koa-helmet')
const { Nuxt, Builder } = require('nuxt')

const app = new Koa()

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(app.env === 'production')
process.dev = config.dev

// load envieronment vars from .env and .env.override; watch the latter for changes
require('./envWithOverrides');

async function start() {
    // Instantiate nuxt.js
    const nuxt = new Nuxt(config)
    await nuxt.ready()

    const {
        host = process.env.HOST || '127.0.0.0',
        port = process.env.PORT || 3000
    } = nuxt.options.server

    // Build in development
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    }

    app.keys = ['3U5HdWbMVGBLOQvpxPaZY1Z'];

    // runs 11 middleware security functions; best to be run early in the stack
    app.use(helmet());

    // merge hardcoded ./configcode/requiredpermissions.js
    // with content of file  ./requiredPermissions.json;
    // and watch this file for changed
    app.use(require('./requiredPermissionsManager')());

    // koa-body parses request body into individual variables
    app.use((require('koa-body'))({ multipart: true  }));

    // tenant middleware assigns ctx.state.tenant
    app.use((require('./tenant.js'))());

    // FB_LOGIN: 
    app.use((require('./api/fblogin.js'))());

    // EXTERNAL_AUTH: if using external authentification, implement it in auth-external.js and uncomment the line below
    app.use((require('./auth-external.js'))());

    // JWT-based authorization middleware
    // implement canLogIn function
    process.status.jwtauthopts = { // this is also used by socket.io further downb
        secret: process.env.JWT_SECRET,
        canLogIn: (user) => true,
        signopts: {
            expiresIn: '90 days'
        }, // see options for jwt.sign https://github.com/auth0/node-jsonwebtoken
    };
    process.env.JWT_SECRET = null // nuxt sends all process.env to client-site; prevent secret from appearing there

    // this one is for koa middleware such as server/auth.js
    const jwtauth = require('./auth-jwt.js');
    app.use(jwtauth(process.status.jwtauthopts));

    // AUTH authentification middleware: /api/auth/login, /api/auth/logout
    app.use(require('./api/authentication.js')(process.status.jwtauthopts));

    // REST api server
    const Api = require('cry-dbapiserver').Server;
    let api = new Api({
        permissions: process.status.requiredPermissions
    });
    app.use(api.run())

    // CMS api server; if not needed, delete pages/_cms directory
    let Cms = require('cry-cmsserver').Server;
    let cms = new Cms({
        permissions: process.status.requiredPermissions
    });
    app.use(cms.run());

    // nuxt server-side rendering
    app.use(ctx => {
        ctx.status = 200
        ctx.respond = false // Bypass Koa's built-in response handling
        ctx.req.ctx = ctx // This might be useful later on, e.g. in nuxtServerInit or with nuxt-stash
        ctx.req.state = ctx.state // This is needed to access user state so that it can be copied to the Vuex store
        nuxt.render(ctx.req, ctx.res)
    })

    // connect and run enterprise bus client
    require('./ebusManager')

    const http = require('http');
    const httpserver = http.createServer(app.callback())

    // connect and run socket.io - continous connection to web browsers
    require('./iosocketManager')(httpserver);

    // start serving web requests
    httpserver.listen(port, host)
    console.log(`Server listening on http://${host}:${port}`);
}

start()
    .catch(error => {
        if (config.dev) {
            console.trace(`koa server error stack trace for ${error.message}`);
        }
        console.error('koa server error: ', error.message);
    });
