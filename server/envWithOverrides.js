/*
    this module:
    - loads environment variables from a .env file into process.env
    - looks for file './.override.env' and injects it over process.env
    - watches the above file for changes, injecting them over current process.env
*/

const envOverridesFile = './.override.env'

const fs = require('fs')
const dotenv = require('dotenv')

let cfg = dotenv.config();

if (process.dev) console.log('configuring ',cfg)

async function loadOverrides() {
    const envConfig = dotenv.parse(fs.readFileSync(envOverridesFile))
    for (let k in envConfig) {
        console.log(`overriding ${k} with ${envConfig[k]}`)
        process.env[k] = envConfig[k]
    }
}

(async function () {  await loadOverrides(); })();

// watch file on disk; if it changes, modify requiredPermissions
let watch = require('node-watch');
watch(envOverridesFile, async function (evt, name) {
    fs.readFile(envOverridesFile, { encoding: 'utf8' }, async (err,data) =>  {
        if (!err) {
            try {
                await loadOverrides();
            } catch(err) {
                console.error(`error reading modified ${envOverridesFile}: ${err.message}`)
            }
        }
    });
});
