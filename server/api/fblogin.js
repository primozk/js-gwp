let axios = require('axios');
let parsJsonl = require('../../code/JSONL');

const Router = require('koa-router');
const router = new Router();

const APP_ID = '2281239845469724';
const APP_SECRET = '0e8079ce214287f1f9eb8f9b21a745c6';
const REDIRECT_URI = process.env.WWW_HOME + 'api/auth/fblogin';

module.exports = function (opts) {

process.env.FB_APP_ID = APP_ID
process.env.FB_REDIRECT_URI = REDIRECT_URI

    router.get('/api/auth/fblogin', async (ctx) => {

        try {

            let code = ctx.request.body.code || ctx.request.query.code;
  
            // exchange code for access token
            let codeExchangeUrl = `https://graph.facebook.com/oauth/access_token?client_id=${APP_ID}&redirect_uri=${REDIRECT_URI}&client_secret=${APP_SECRET}&code=${code}`;
            let accessTokenRes = await axios.post(codeExchangeUrl);
            let fb_access_token = accessTokenRes.data.access_token;
            let fb_access_token_expires = accessTokenRes.data.expires_in;
            ctx.state.fb_access_token = fb_access_token;
            ctx.state.fb_access_token_expires = fb_access_token_expires;

            // obtain personal data
            let personalInfoUrl = `https://graph.facebook.com/me?fields=name,email&access_token=${fb_access_token}`;
            let personalInfoRes = await axios.post(personalInfoUrl);

            let fb_personal_data = personalInfoRes.data;
            ctx.state.fb_personal_data = fb_personal_data;

            console.log('FB state', ctx.state)

            console.log('ctx.state.fb_personal_data',ctx.state.fb_personal_data)
            console.log('FB store', ctx.store)


        } catch (err) {
            console.log('fblogin error: ',err)
        }

    });


    return router.routes();
}
