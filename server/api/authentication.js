const Router = require('koa-router');
//const Repo = require('cry-db').Repo;
const UserMgr = require('cry-usermgr');
const userMgr = new UserMgr();
const router = new Router();
const decodeuser = require('../../code/decodeuser');
const Can = require('cry-can');
const can = new Can('users', 'admin');

module.exports = function (opts) {

    router.post('/api/auth/login', async (ctx) => {
        const {
            username,
            password
        } = ctx.request.body

        let user = await userMgr.getActive(username,password);

        user = decodeuser(user, opts)

        if (user) {
            const accessToken = user.token;
            ctx.cookies.set('auth',accessToken) // set another token for API use
            ctx.status = 200
            ctx.body = user
        } else {
            ctx.status = 403
            ctx.body = {
                error: `user ${username} cannot login with provided password`
            }
        }
    });

    router.get('/api/auth/user', async (ctx) => {
        const { username } = ctx.request.body
        if (username) {
            if (!can.can(process.status.requiredPermissions,ctx.state.user.permissions,'get')) {
                ctx.status=403;
                return
            }
            ctx.status=200;
            ctx.body = await userMgr.get(username);
        }

        ctx.body = {
            user: ctx.state.user
        }
    });

    router.post('/api/auth/user', async (ctx) => {
        if (!can.can(process.status.requiredPermissions,ctx.state.user.permissions,'edit')) {
            ctx.status=403
            return
        }
        ctx.status = 200;
        ctx.body = await userMgr.save(ctx.request.body);
    });

    router.post('/api/auth/block', async (ctx) => {
        if (!can.can(process.status.requiredPermissions,ctx.state.user.permissions,'block')) {
            ctx.status=403
            return
        }
        ctx.status = 200;
        ctx.body = await userMgr.block(ctx.request.body);
    });

    router.post('/api/auth/block', async (ctx) => {
        if (!can.can(process.status.requiredPermissions,ctx.state.user.permissions,'activate')) {
            ctx.status=403
            return
        }
        ctx.status=200;
        ctx.body = await userMgr.activate(ctx.request.body);
    });


    router.get('/api/auth/logout', async (ctx) => {
        ctx.cookies.set('auth',null)
        ctx.redirect('/')
    });

    router.post('/api/auth/logout', async (ctx) => {
        ctx.cookies.set('auth',null)
        ctx.body = {
            status: 'OK'
        }
    });

    router.get('/api/auth/init', async (ctx) => {

        if (!can.can(process.status.requiredPermissions,ctx.state.user.permissions,'init')) {
            ctx.status=403
            return
        }

        let admin = await userMgr.getActive('admin');
        if (!admin) {
            await userMgr.save({
                username: 'admin',
                password: 'admin',
                permissions: {
                    'users.admin': true
                }
            })
        }
        ctx.redirect('/');
    });

    return router.routes();
}
