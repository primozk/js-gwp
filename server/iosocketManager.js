
const forwardAuthorizationInfoToEbusWorkers = true;

module.exports = (httpserver) => {
    // socket.io connection to web clients
    const Can = require('cry-can');
    const socket = require('socket.io');
    const io = socket(httpserver)
    const decodeuser = require('../code/decodeuser.js');
    const canServe = new Can();

    const socketConnections = new Map()


    process.status = process.status || {}
    process.status.io = io

    process.status.io.clientAnnounce = async function(channel,message) {
        socketConnections.forEach( (value,key) => {
            if (value.subscriptions) {
                value.subscriptions.forEach(sub => {
                    if (channel.startsWith(sub)) {
                        value.socket.emit('subscriptionMessage',{ channel, message })
                    }
                })
            }
        })
    }

    io.on('connection', async (socket) => {

        socketConnections.set(socket.id,{ socket })

        if (process.status.ebus) {
            socket.emit( process.status.ebus.online ? 'ebus online' : 'ebus offline' );
            socket.on('serve', async (data) => {
                if (data && typeof data === 'object' && data.service) {
                    let user = {}
                    if (data.token) {
                        user = decodeuser(data.token, process.status.jwtauthopts);
                        delete user.token;
                    };
                    // check permissions for this service
                    let operation = (data.data && data.data.operation) || 'call'
                    let canExec = canServe.can(process.status.requiredPermissions, user.permissions, operation, data.service, 'ebus');
                    if (data.data && data.data.collection) {
                        canExec = canExec && canServe.can(process.status.requiredPermissions, user.permissions, operation, data.data.collection, data.service)
                    }
                    if (canExec) {
                        try {
                            if (forwardAuthorizationInfoToEbusWorkers && user.username) {
                                data.data._auth = {
                                    username: user.username,
                                    permissions: user.permissions,
                                    requiredPermissions: process.status.requiredPermissions
                                }
                            }
                            let response = await process.status.ebus.client.requestPromise(data.service, data.data, data.rid, data.stales)
                            socket.emit('serviceResult', response)
                        } catch (error) {
                            socket.emit('serviceError', error)
                        }
                    } else {
                        console.error('user not authorized to call ebus worker ',user,data)
                    }
                } else {
                    console.error(`client not allowed to execute service ${data.service}`)
                }
            }); // serve
            socket.on('subscribe', async (data) => {
                if (data && typeof data === 'object' && data.channel) {
                    let user = {}
                    if (data.token) {
                        user = decodeuser(data.token, process.status.jwtauthopts);
                        delete user.token;
                    };
                    // check permissions for this service
                    let canExec = canServe.can(process.status.requiredPermissions, user.permissions, data.channel, 'subscribe', 'ebus');
                    if (canExec) {
                        let c = socketConnections.get(socket.id);
                        c.user = user;
                        c.subscriptions = c.subscriptions || new Set()
                        c.subscriptions.add(data.channel)
                    } else {
                        console.error('user not authorized to subscribe ',user,data)
                    }
                }
            }); // subscribe
        }
        socket.on('disconnect', async (reason) => {
            socketConnections.delete(socket.id)
        });
    });

    return httpserver;
}
