module.exports = () => {
    const middleware = async function (ctx, next) {
        ctx.state.tenant = ctx.get('tenant') || ctx.cookies.get('tenant') || process.env.DEFAULT_TENANT;
        await next();
    }
    return middleware;
}
