/*
    this koa middleware:
    - reads hardcoded required permissions object from ../configcode/requiredpermissions
    - looks for file './requiredPermissions.json' and injects it over hard-coded required permissions object
    - watches the above file for changes, injecting them over current required permissions object
*/

const fs = require('fs')
const requiredPermissionsFile = './requiredPermissions.json'

process.status = process.status || {}
process.status.requiredPermissions = require('../configcode/requiredpermissions').permissions;

// get required permissions file and monitor it for changes
try {
    let retStr = fs.readFileSync(requiredPermissionsFile, { encoding: 'utf8' });
    let retObj = JSON.parse(retStr);
    process.status.requiredPermissions = { ...process.status.requiredPermissions,...retObj }
} catch(err) {
    console.error(`error reading ${requiredPermissionsFile}: ${err.message}`)
}

// watch file on disk; if it changes, modify requiredPermissions
let watch = require('node-watch');
watch(requiredPermissionsFile, function (evt, name) {
    fs.readFile(requiredPermissionsFile, { encoding: 'utf8' }, (err,data) =>  {
        if (!err) {
            try {
                let retObj = JSON.parse(data);
                process.status.requiredPermissions = { ...process.status.requiredPermissions,...retObj }
                if (process.dev) console.log('re-reading ',requiredPermissionsFile,retObj)
            } catch(err) {
                console.error(`error reading modified ${requiredPermissionsFile}: ${err.message}`)
            }
        }
    });
});
// copy requiredPermissions to ctx.state; store/index.js will copy this to $store.state.requiredPermissions

module.exports = (opts = {}) => {
    const middleware = async function (ctx, next) {
        ctx.state.requiredPermissions = process.status.requiredPermissions;
        await next();
    }
    return middleware;
}
