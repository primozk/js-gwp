
let jsonwebtoken = require('jsonwebtoken');

function decode(token,secret) {
    if (Array.isArray(secret)) {
        return secret.find( s => decode(token,s));
    }
    try {
        return jsonwebtoken.verify(token,secret);
    }
    catch(err) {
        return null;
    }
}

function getuser(token,opts) {

    if (!token) return token;

    let user = typeof token==='object'
        ? token
        : (decode(token,opts.secret) || {})

    if (typeof opts.canLogIn==='function') {
        user.status = opts.canLogIn(user);
    }

    // if user is disabled (status evals to false), log it out
    if (!user.status || !user.username) {
        user={}
        token=null;
    } else {
        // cleanup user object
        if (typeof token==='object') {
            token = jsonwebtoken.sign( user, opts.secret, opts.signopts )
        }
        user.token = token;
        delete user.password;
        delete user.passhash;
        delete user.passsalt;
        delete user.status;
        delete user.iat;            // issued-at; to get the date when token was issued: let tokenIssued = new Date(user.iat*1000)
    }

    return user;
}

module.exports = getuser;
