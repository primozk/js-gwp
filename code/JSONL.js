
function parseJsonl(text) {
    
  let r = /(?<name>[A-Za-z0-9_]+)(\s*\:\s*)(\'(?<val>[^\']*)\')*/gim;
  p = text.match(r)

  if (p) {
    let res = {}
    p.forEach(match => {
      let semicol = match.indexOf(':');
      let name = match.substring(0,semicol).trim();
      let val = match.substring(semicol+1).trim();
      val = val.substring(1,val.length-1)
      res[name]=val;
    });
    console.log(res)
    return res;
  }

  return null;
};

module.exports  = parseJsonl;
