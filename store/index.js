import { normalizeRule } from "webpack/lib/RuleSet";

export default {
    state: () => ({
        user: {},
        tenant: null,
        ebus: {
            online: undefined
        },
        server: {
            online: true
        },
    }),
    mutations: {
        SET_USER(state, user) {
            state.user = user
        },
        SET_SERVER_ONLINE(state, online) {
            state.server.online = online
        },
        SET_EBUS_ONLINE(state, online) {
            state.ebus.online = online
        },
        SET_REQUIRED_PERMISSONS(state, permissions) {
            state.requiredPermissions = permissions
        },
        SET_TENANT(state,tenant) {
            state.tenant = tenant;
        },
    },
    actions: {
        nuxtServerInit({commit}, ctx) {
            if (ctx.req.state) {
                commit('SET_USER', ctx.req.state.user);
                commit('SET_REQUIRED_PERMISSONS', ctx.req.state.requiredPermissions);
            }
            commit('SET_TENANT', ctx.req.state.tenant);
            commit('SET_EBUS_ONLINE', process.status.ebus.online);
        },
    }
}
