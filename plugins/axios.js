export default function ({ $axios, redirect }) {
    $axios.onRequest((config) => {
        console.log('AXIOS base url',config.baseURL)
        console.log('AXIOS Making request to ' + config.url,'from',process.server?"SERVER":"CLIENT",'with baseURL',config.baseURL)
    })

    // $axios.onError(error => {
    //   const code = parseInt(error.response && error.response.status)
    //   if (code === 400) {
    //     redirect('/400')
    //   }
    // })
  }
