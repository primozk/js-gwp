import Vue from 'vue'

export default function () {
    Vue.mixin({
        methods: {
            capitalize(str) {
                if (str && typeof(str)==='string' && str.length) return str.charAt(0).toUpperCase() + str.slice(1);
                else return str;
            }
        },
        filters: {
            capitalize(str) {
                if (str && typeof(str)==='string' && str.length) return str.charAt(0).toUpperCase() + str.slice(1);
                else return str;
            }
        }
    });
  }




