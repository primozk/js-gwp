import Vue from 'vue'
import socket from 'socket.io-client'
import helpers from 'cry-helpers'

const uuid = require('uuid/v1');
const DefferedPromise = helpers.DefferedPromise;

let iorequests = new Map()
let iosocket = socket();

export default function () {
    Vue.mixin({
        data() {
            return {
                iosocket: null
            }
        },
        methods: {
            async callEbus(service,data) {
                const promise = new DefferedPromise();
                promise.rid = uuid()
                iorequests.set(promise.rid,promise);

                let payload = {
                    rid: promise.rid,
                    service,
                    token: this.$store.state.user.token,
                    data
                }
                this.iosocket.emit('serve', payload)

                return promise;
            },
            async callEbusOperation(service,operation,data={}) {
                data.operation=operation;
                return this.callEbus(service,data)
            }
        },
        created: function() {
            this.iosocket = iosocket
            this.iosocket.on('serviceResult', data => {
                if (!data.rid) return;
                let promise=iorequests.get(data.rid)
                if (!promise) return;
                promise.resolve(data.data)
                iorequests.delete(data.rid)

            })
            this.iosocket.on('serviceError', data => {
                if (!data.rid) return;
                let promise=iorequests.get(data.rid)
                if (!promise) return;
                promise.reject(data.data)
                iorequests.delete(data.rid)
            })
            this.iosocket.on('connect', () => { this.$store.commit('SET_SERVER_ONLINE',true) })
            this.iosocket.on('disconnect', () => { this.$store.commit('SET_SERVER_ONLINE',false) })
            this.iosocket.on('reconnecting', () => { this.$store.commit('SET_SERVER_ONLINE',false) })
            this.iosocket.on('ebus online', () => { this.$store.commit('SET_EBUS_ONLINE',true) })
            this.iosocket.on('ebus offline', () => { this.$store.commit('SET_EBUS_ONLINE',false) })
        }
    });
  }

