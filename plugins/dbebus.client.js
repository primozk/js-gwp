import Vue from 'vue'
import helpers from 'cry-helpers'

const DB_SERVICE='db'
let db = {}

export default function () {
    Vue.mixin({
        data() {
            return {
            }
        },
        methods: {
            async dbPing()
            {
                return this.callEbusForDb('ping')
            },
            async dbNewid()
            {
                return this.callEbusForDb('newid')
            },
            async dbFind(collection,query)
            {
                return this.callEbusForDb('find',{ collection, query })
            },
            async dbFindOne(collection,query)
            {
                return this.callEbusForDb('findOne',{ collection, query })
            },
            async dbUpsert(collection,query,update) {
                delete update._id;
                return this.callEbusForDb('upsert',{ collection, query, update })
            },
            async dbUpdate(collection,query,update) {
                delete update._id;
                return this.callEbusForDb('update',{ collection, query, update })
            },
            async dbUpdateOne(collection,query,update) {
                delete update._id;
                return this.callEbusForDb('updateOne',{ collection, query, update })
            },
            async dbDelete(collection,query,update) {
                return this.callEbusForDb('delete',{ collection, query })
            },
            async dbDeleteOne(collection,query,update) {
                return this.callEbusForDb('deleteOne',{ collection, query })
            },
            async dbGetChanges(collection,id) {
                return id 
                    ? this.callEbusForDb('getChangesForEntity',{ collection, id })
                    : this.callEbusForDb('getChangesForCollection',{ collection })
            },

            async callEbusForDb(operation,data) {
                if (this.tenant) data.db = this.tenant;
                return this.callEbusOperation(DB_SERVICE,operation,data)
            },

            dbGetCollection(collection) {
                if (db[collection]) return db[collection];
                Vue.set(db,collection,{})
                return db[collection]
            },

            async dbLoad(collection,query={}) {
                let col = {}
                let data = await this.dbFind(collection,query)
                data.forEach( rec => col[rec._id]=rec );
                Vue.set(db, collection, col);
                return db[collection];
            },
            async dbGetEntity(collection,_id)
            {
                let col=dbGetCollection(collection);
                if (col[_id]) return col[_id];
                Vue.set(col,_id,await this.dbFindOne(collection,{_id}))
                return col[_id];
            }

        },
        created () {
            this.iosocket.emit('subscribe',{channel:'db'});
            this.iosocket.on('subscriptionMessage',subdata => {
                if (subdata.dbebusprocessed) return;
                subdata.dbebusprocessed=true;
                let collection = subdata.message.collection;
                let operation = subdata.message.operation;
                let data = subdata.message.data;
                let col = this.dbGetCollection(collection);
                if (operation==="update" || operation==="insert") {
                    Vue.set(col,data._id,data)
                    return
                }
                if (operation==="delete") {
                    Vue.delete(col,data._id)
                    return
                }
                if (operation==="deleteMany" || operation==="updateMany") {
                    console.log(operation)
                    this.dbLoad(collection)
                    return
                }
                console.error('unsupported db update operation',operation)
            })
        },
    });
  }

