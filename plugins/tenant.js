import Vue from 'vue'

export default function () {
    Vue.mixin({
        data() {
            return {
                tenant: this.$store &&  this.$store.state && this.$store.state.tenant || "no tenant"
            }
        },
    });
  }
