
export default ({app,store}) => {
    app.callEbus = async function(service,data) {
        let ret = await process.status.ebus.serve(service,data,store.state.user)
        return ret.data;
    };
    app.callEbusOperation = async function(service,operation,data={}) {
        data.operation=operation;
        return callEbus(service,data)
    };
}
