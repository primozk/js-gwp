import Vue from 'vue'
import Can from 'cry-can'

let can = new Can();

export default function (ctx,store) {

    Vue.mixin({
        data() {
            return {
                user: this.$store && this.$store.state && this.$store.state.user || {}
            }
        },
        methods: {
            can(operation,object,module) {
                let requiredPermissions =  this.$store.state.requiredPermissions;
                let userPermissions = this.user.userPermissions;
                console.log('can',requiredPermissions,userPermissions)
                return can.can(requiredPermissions,userPermissions,operation,object,module);
            }
        },
    });
  }




