import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAOxvNkInGfygem99LpefWR0flujIzjjos",
    libraries: "places"
  }
});
