export default function (ctx) {
    console.log('AUTH', ctx.store.state)
    let user = ctx.store.state.user
    console.log('auth',ctx.route.path)
    if (!user || !user.username) {
        console.log('REDIRECTING ',ctx.route.path)
        ctx.redirect('/login?ret='+ctx.route.path)
    }
}
